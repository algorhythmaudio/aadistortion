#include "AADistortion.h"



#pragma mark - Shaper 1

void waveShaperBuffers1 (float *inBuff, float *outBuff, unsigned int buffSize, float distAmount) {
    if (buffSize <= 0 || inBuff == NULL || distAmount < 1.0) return;

    for (int i=0; i<buffSize; i++)
        outBuff[i] = inBuff[i] * (fabsf(inBuff[i] + distAmount) / ((inBuff[i]*inBuff[i]) + (distAmount-1) * fabsf(inBuff[i]) + 1));
}


void waveShaperBuff1 (float *buff, unsigned int buffSize, float distAmount) {
    if (buffSize <= 0 || buff == NULL || distAmount < 1.0) return;
    float samp;
    
    for (int i=0; i<buffSize; i++) {
        samp = buff[i];
        buff[i] = samp * (fabsf(samp + distAmount) / ((samp*samp) + (distAmount-1) * fabsf(samp) + 1));
    }
}


float waveShaper1 (float *inSamp, float distAmount) {
    if (distAmount < 1.0) return inSamp[0];
    
    return inSamp[0] * (fabsf(inSamp[0] + distAmount) / ((inSamp[0]*inSamp[0]) + (distAmount-1) * fabsf(inSamp[0]) + 1));
}





#pragma mark Shaper 2


void waveShaperBuffers2 (float *inBuff, float *outBuff, unsigned int buffSize, float distAmount) {
    if (inBuff == NULL || buffSize <= 0) return;
    
    for (int i=0; i<buffSize; i++)
        outBuff[i] = ((1+distAmount) * inBuff[i]) - (distAmount * inBuff[i] * inBuff[i] * inBuff[i]);
}


void waveShaperBuff2 (float *buff, unsigned int buffSize, float distAmount) {
    if (buff == NULL || buffSize <= 0) return;
    float samp;
    
    for (int i=0; i<buffSize; i++) {
        samp = buff[i];
        buff[i] = ((1+distAmount) * samp) - (distAmount * samp * samp * samp);
    }
}


float waveShaper2 (float *inSamp, float distAmount) {
    return ((1+distAmount) * inSamp[0]) - (distAmount * inSamp[0] * inSamp[0] * inSamp[0]);
}






#pragma mark Shaper 3


void waveShaperBuffers3 (float *inBuff, float *outBuff, unsigned int buffSize, float distAmount) {
    if (buffSize <= 0 || inBuff == NULL || distAmount == 1.0) return;
    
    float k = (2 * distAmount) / (1-distAmount);
    
    for (int i=0; i<buffSize; i++)
        outBuff[i] = (1+k) * inBuff[i] / (1 + k*fabsf(inBuff[i]));
}


void waveShaperBuff3 (float *buff, unsigned int buffSize, float distAmount) {
    if (buff == NULL || buffSize <= 0 || distAmount == 1.0) return;
    
    float samp;
    float k = (2*distAmount) / (1-distAmount);
    
    for (int i=0; i<distAmount; i++) {
        samp = buff[i];
        buff[i] = (1+k) * samp / (1 + k*fabsf(samp));
    }
}


float waveShaper3 (float *inSamp, float distAmount) {
    if (distAmount == 1.0) return inSamp[0];
    
    float k = (2 * distAmount) / (1-distAmount);
    return (1+k) * inSamp[0] / (1 + k*fabsf(inSamp[0]));
}




#pragma mark - Foldback Distorion 1


void foldDistBuffers1 (float *inBuff, float *outBuff, unsigned int buffSize, float thresh) {
    if (inBuff == NULL || buffSize <= 0 || thresh <= 0) return;
    
    for (int i=0; i<buffSize; i++) {
        if (inBuff[i] > thresh || inBuff[i] < -thresh)
            outBuff[i] = fabsf(fabsf(fmodf(inBuff[i] - thresh, thresh*4)) - thresh*2) - thresh;
    }
}


void foldDistBuff1 (float *buff, unsigned int buffSize, float thresh) {
    if (buff == NULL || buffSize <= 0 || thresh <= 0) return;
    
    float samp;
    
    for (int i=0; i<buffSize; i++) {
        samp = buff[i];
        if (samp > thresh || samp < -thresh)
            buff[i] = fabsf(fabsf(fmodf(samp-thresh, thresh*4)) - thresh*2) - thresh;
    }
}


float foldDist1 (float *inSamp, float thresh) {
    if (thresh <= 0) return inSamp[0];
    
    return fabsf(fabsf(fmodf(inSamp[0] - thresh, thresh*4)) - thresh*2) - thresh;
}































