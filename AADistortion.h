/*
 *	File:		AADistortion.h
 *
 *	Version:	1.2.0
 *
 *	Created:	12-10-05 by GW
 *	Updated:	12-11-14 by GW
 *
 *	Copyright:  Copyright © 2012 Algorthythm Audio, All rights reserved.
 *
 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
 *
 *  @Summery
 *          This is a set of wave shaping functions.  Each type of wave shaper has three types of functions
 *      based on the need of the developer.  For the three wave shapers there's a function pointer to make
 *      automating between different types easier.  But note that a distAmmount for one type, doesn't 
 *      match the ammount for another.
 *
 *          There's also a foldback distortion wave shaper.  This is different than the others in that it
 *      needs a threshold arument and only augments samples above the threshold.
 *
 *  @Usage Example
 *      dist_buff distortion = waveShaperBuff1;
 *      distortion(inBuff, buffSize, ammount);
 *
 *~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
*/


#ifndef __AADistortion__AADistortion__
#define __AADistortion__AADistortion__

#include <iostream>
#include <math.h>


enum DistTypes {
    WShaper1,
    WShaper2,
    WShaper3,
    FoldDist1
};



// function pointers for each distortion type
typedef void (*dist_buffers) (float *inBuff, float *outBuff, unsigned int buffSize, float distAmount);
typedef void (*dist_buff) (float *buff, unsigned int buffSize, float distAmount);
typedef float (*dist_sample) (float *inSamp, float distAmount);



// Expect value of inBuff/inSamp to be [-1...1]
// distAmount >= 1
void waveShaperBuffers1     (float *inBuff, float *outBuff, unsigned int buffSize, float distAmount);
void waveShaperBuff1        (float *buff, unsigned int buffSize, float distAmount);
float waveShaper1           (float *inSamp, float distAmount);


// Expects values of inBuff/inSamp to be [-1...1]
// dist ammount 0>x>1
void waveShaperBuffers2     (float *inBuff, float *outBuff, unsigned int buffSize, float distAmount);
void waveShaperBuff2        (float *buff, unsigned int buffSize, float distAmount);
float waveShaper2           (float *inSamp, float distAmount);


// Expects values of inBuff/inSamp to be [-1...1]
// distAmount CANNOT be 1
void waveShaperBuffers3     (float *inBuff, float *outBuff, unsigned int buffSize, float distAmount);
void waveShaperBuff3        (float *buff, unsigned int buffSize, float distAmount);
float waveShaper3           (float *inSamp, float distAmount);







// foldback distortion is based on a threshold, the threshold > 0
void foldDistBuffers1       (float *inBuff, float *outBuff, unsigned int buffSize, float thresh);
void foldDistBuff1          (float *buff, unsigned int buffSize, float thresh);
float foldDist1             (float *inSamp, float thresh);













#endif







